package com.example.kss.endpoint;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.example.kss.logic.ExampleLogic;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.kss.model.Activate;
import com.example.kss.model.GetInfo;
import com.example.kss.model.HardCancel;
import com.example.kss.util.HttpUtil;

@Component
@Path("/subscription") //必要設定
public class ExampleEndPoint {
	
	private static Logger log = LogManager.getLogger(HttpUtil.class);
	
	@Autowired
	private ExampleLogic exampleLogic;
	
	/**
	 * return HTTP code 200 means the service is available.
	 * @return
	 */
	@GET
	@Path("/ping")
	@Consumes(MediaType.WILDCARD)
	@Produces({MediaType.TEXT_PLAIN})
    public String ping() {
        return "pong";
    }
	
	/*
	 * Buy a subscription and become a Subscriber
	 */
	@POST
	@Path("/activation")
	@Consumes({MediaType.APPLICATION_JSON}) 
    public Response activate (@Valid Activate activate) throws Exception {
		log.error("thread id={}, start json={}", Thread.currentThread().getId(), activate.toString());
		String respJson = exampleLogic.activate(activate);
        return Response.status(Response.Status.OK)
	    	      .entity(respJson)
	    	      .type(MediaType.APPLICATION_JSON)
	    	      .build();
    }
	
	/**
	 * Cancel a subscription
	 * @param hardCancel
	 * @return
	 * @throws Exception
	 */
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/hard-cancel")
	public Response cancel(@Valid HardCancel hardCancel) throws Exception {
		log.error("thread id={}, start json={}", Thread.currentThread().getId(), hardCancel.toString());
		String respJson = exampleLogic.hardCancel(hardCancel);
		return Response.status(Response.Status.OK)
				.entity(respJson)
				.type(MediaType.APPLICATION_JSON)
				.build();
	}
	
	/**
	 * get subscription detail
	 * @param getInfo
	 * @return
	 * @throws Exception
	 */
	@GET
	@Consumes({MediaType.APPLICATION_JSON})
	@Path("/info")
    public Response getInfo(@Valid GetInfo getInfo) throws Exception {
		log.error("thread id={}, start json={}", Thread.currentThread().getId(), getInfo.toString());
		String respJson = exampleLogic.getInfo(getInfo);
		return Response.status(Response.Status.OK)
				.entity(respJson)
				.type(MediaType.APPLICATION_JSON)
				.build();
    }
	
}
