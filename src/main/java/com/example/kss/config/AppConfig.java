package com.example.kss.config;

import com.example.kss.endpoint.ExampleEndPoint;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/**
 * https://docs.spring.io/spring-boot/docs/2.2.1.RELEASE/reference/htmlsingle/#boot-features-jersey
 *
 */
@Component
public class AppConfig extends ResourceConfig {
	
	public AppConfig() {
		//註冊class的方式
		register(ExampleEndPoint.class);
		//註冊package的方式
//		packages("com.example.kss.endpoint");
    }
}
