package com.example.kss.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class KssSystemConfig {
	private static Logger log = LogManager.getLogger(KssSystemConfig.class);
	
	public static String URI;
	@Value("${kss.request.uri}")
	public void setURI(String uri) {
		KssSystemConfig.URI = uri;
	}
	
	public static String PATH;
	@Value("${kss.cert.path}")
	public void setPath(String path) {
		this.PATH = path;
	}

	public static String CERT_FILE_NAME;
	@Value("${kss.cert.name}")
	public void setCertFileName(String certFileName) {
		this.CERT_FILE_NAME = certFileName;
	}

	public static String ACCESS_INFO_NAME;
	@Value("${kss.accessinfo.name}")
	public void setAccessInfoName(String accessInfoName) {
		this.ACCESS_INFO_NAME = accessInfoName;
	}

	public static String ACCESS_INFO_PWD;
	@Value("${kss.accessinfo.password}")
	public void setAccessInfoPwd(String accessInfoPwd) {
		this.ACCESS_INFO_PWD = accessInfoPwd;
	}

}
