package com.example.kss.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "AccessInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccessInfo {
	
	@XmlElement(name = "UserName") //<UserName>XXX</UserName>
	private String userName;
	
	@XmlElement(name = "Password")
	private String passWord;

	public AccessInfo() {
		super();
	}

	public AccessInfo(String userName, String passWord) {
		super();
		this.userName = userName;
		this.passWord = passWord;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

}
