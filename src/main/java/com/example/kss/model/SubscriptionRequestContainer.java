package com.example.kss.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SubscriptionRequestContainer")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubscriptionRequestContainer  {
	
	@XmlElement(name = "Timestamp")
	private String timeStamp;
	@XmlElement(name = "TransactionId")
	private String transactionId;
	@XmlElement(name = "SubscriptionRequest")
	private SubscriptionRequest subscriptionRequest;

	public SubscriptionRequestContainer() {
		super();
	}

	public SubscriptionRequestContainer(String timeStamp, String transactionId, SubscriptionRequest subscriptionRequest) {
		super();
		this.timeStamp = timeStamp;
		this.transactionId = transactionId;
		this.subscriptionRequest = subscriptionRequest;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public SubscriptionRequest getSubscriptionRequest() {
		return subscriptionRequest;
	}

	public void setSubscriptionRequest(SubscriptionRequest subscriptionRequest) {
		this.subscriptionRequest = subscriptionRequest;
	}

}
