package com.example.kss.model;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlAccessorType(XmlAccessType.FIELD)
public class Renew {
	
	@NotNull(message = "UnitId can not be null or empty")
	@XmlAttribute(name="UnitId")
	@JsonProperty("unitld")
	private String unitld;
	
	@NotNull(message = "SubscriberId can not be null or empty")
	@XmlAttribute(name="SubscriberId")
	@JsonProperty("subscriberId")
	private String subscriberId;
	
	@NotNull(message = "EndTime can not be null or empty")
	@XmlAttribute(name="EndTime")
	@JsonProperty("endTime")
	private String endTime;

	public Renew() {
		super();
	}

	public Renew(String unitld, String subscriberId, String endTime) {
		super();
		this.unitld = unitld;
		this.subscriberId = subscriberId;
		this.endTime = endTime;
	}

	public String getUnitld() {
		return unitld;
	}

	public void setUnitld(String unitld) {
		this.unitld = unitld;
	}

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "Renew [unitld=" + unitld + ", subscriberId=" + subscriberId + ", endTime=" + endTime + "]";
	}

}
