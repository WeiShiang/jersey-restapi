package com.example.kss.model;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlAccessorType(XmlAccessType.FIELD)
public class GetInfo {
	@NotNull(message = "UnitId can not be null or empty")
	@XmlAttribute(name = "UnitId")
	@JsonProperty("unitId")
	private String unitId;

	@NotNull(message = "SubscriberId can not be null or empty")
	@XmlAttribute(name = "SubscriberId")
	@JsonProperty("subscriberId")
	private String subscriberId;

	@NotNull(message = "InfoSection can not be null or empty")
	@XmlAttribute(name = "InfoSection")
	@JsonProperty("infoSection")
	private String infoSection;

	public GetInfo() {
		super();
	}

	public GetInfo(String unitId, String subscriberId, String infoSection) {
		super();
		this.unitId = unitId;
		this.subscriberId = subscriberId;
		this.infoSection = infoSection;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public String getInfoSection() {
		return infoSection;
	}

	public void setInfoSection(String infoSection) {
		this.infoSection = infoSection;
	}

}
