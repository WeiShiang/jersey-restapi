package com.example.kss.model;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlAccessorType(XmlAccessType.FIELD)
public class Activate {

	@NotNull(message = "UnitId can not be null or empty")
	@XmlAttribute(name = "UnitId")
	@JsonProperty("unitId")
	private String unitId;

	@NotNull(message = "SubscriberId can not be null or empty")
	@XmlAttribute(name = "SubscriberId")
	@JsonProperty("subscriberId")
	private String subscriberId;

	@NotNull(message = "StartTime can not be null or empty")
	@XmlAttribute(name = "StartTime")
	@JsonProperty("startTime")
	private String startTime;

	@NotNull(message = "EndTime can not be null or empty")
	@XmlAttribute(name = "EndTime")
	@JsonProperty("endTime")
	private String endTime;

	@XmlAttribute(name = "LicenseCount")
	@JsonProperty("licenseCount")
	private String licenseCount;

	@NotNull(message = "ProductId can not be null or empty")
	@XmlAttribute(name = "ProductId")
	@JsonProperty("productId")
	private String productId;

	public Activate() {
		super();
	}

	public Activate(String unitId, String subscriberId, String startTime, String endTime, String licenseCount,
			String productId) {
		super();
		this.unitId = unitId;
		this.subscriberId = subscriberId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.licenseCount = licenseCount;
		this.productId = productId;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getLicenseCount() {
		return licenseCount;
	}

	public void setLicenseCount(String licenseCount) {
		this.licenseCount = licenseCount;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "Activate [unitId=" + unitId + ", subscriberId=" + subscriberId + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", licenseCount=" + licenseCount + ", productId=" + productId + "]";
	}

}
