@javax.xml.bind.annotation.XmlSchema(namespace = "http://sever:port/services/subscription/",
									 elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED,
									 xmlns = {@javax.xml.bind.annotation.XmlNs(namespaceURI = "http://sever:port/services/subscription/", prefix = "")})
package com.example.kss.model;
/**
 *  this class is for remove XML prefix ns2: and add xml namespace
 *  
 *  e.g.
 *	<ns2:foo xmlns:ns2="http://namespace" /> 
 * 					to 
 * 	<foo xmlns="http://namespace" />
 *
 */
