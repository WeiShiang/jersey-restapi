package com.example.kss.model;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlAccessorType(XmlAccessType.FIELD)
public class HardCancel {
	
	@NotNull(message = "UnitId can not be null or empty")
	@XmlAttribute(name="UnitId")
	@JsonProperty("unitId")
	private String unitId;
	
	@NotNull(message = "SubscriberId can not be null or empty")
	@XmlAttribute(name="SubscriberId")
	@JsonProperty("subscriberId")
	private String subscriberId;
	
	@NotNull(message = "EndTime can not be null or empty")
	@XmlAttribute(name="EndTime")
	@JsonProperty("endTime")
	private String endTime;

	public HardCancel() {
		super();
	}

	public HardCancel(String unitId, String subscriberId, String endTime) {
		super();
		this.unitId = unitId;
		this.subscriberId = subscriberId;
		this.endTime = endTime;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "HardCancel [unitId=" + unitId + ", subscriberId=" + subscriberId + ", endTime=" + endTime + "]";
	}

}
