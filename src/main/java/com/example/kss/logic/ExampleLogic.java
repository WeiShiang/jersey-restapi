package com.example.kss.logic;

import java.time.Instant;
import java.util.Iterator;
import java.util.UUID;

import javax.validation.Valid;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPMessage;

import com.example.kss.config.KssSystemConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.example.kss.model.AccessInfo;
import com.example.kss.model.Activate;
import com.example.kss.model.GetInfo;
import com.example.kss.model.HardCancel;
import com.example.kss.model.Renew;
import com.example.kss.model.SubscriptionRequest;
import com.example.kss.model.SubscriptionRequestContainer;
import com.example.kss.util.HttpUtil;
import com.example.kss.util.SoapUtil;

@Component
public class ExampleLogic {
	
	private static Logger log = LogManager.getLogger(ExampleLogic.class);
	
	public static void main(String[] args) {
		try {
			Instant timestamp = Instant.now();
			System.out.println(timestamp.toString());
//			System.out.println(LocalDateTime.now().toString()); 
//			System.out.println(LocalDateTime.now(ZoneOffset.UTC).toString()); 
//			System.out.println(LocalDateTime.now().atZone(ZoneId.of("UTC")).toInstant().toString()); 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String activate(Activate activate) throws Exception {

		String transactionId = UUID.randomUUID().toString();
		String nowTime = Instant.now().toString();

		//request string
		SubscriptionRequestContainer container = new SubscriptionRequestContainer(nowTime, transactionId, new SubscriptionRequest(activate));
		AccessInfo accessInfo = new AccessInfo (KssSystemConfig.ACCESS_INFO_NAME, KssSystemConfig.ACCESS_INFO_PWD);
		String str = SoapUtil.getInstance().generateSoapMsgStr(accessInfo, container);
		
		//send request
		String response = HttpUtil.doPost(KssSystemConfig.URI, str);
		
		//response
		SOAPMessage msg = SoapUtil.getInstance().strXMLToSoapMsg(response);
		SOAPBody body = msg.getSOAPBody();
		Iterator<SOAPElement> iterator = body.getChildElements();
		return SoapUtil.getInstance().xmlToJson(iterator, "Activate");
	}
	
	public String hardCancel(HardCancel hardCancel) throws Exception {
		String transactionId = UUID.randomUUID().toString();
		String nowTime = Instant.now().toString();

		//request string
		SubscriptionRequestContainer container = new SubscriptionRequestContainer(nowTime, transactionId, new SubscriptionRequest(hardCancel));
		AccessInfo accessInfo = new AccessInfo (KssSystemConfig.ACCESS_INFO_NAME, KssSystemConfig.ACCESS_INFO_PWD);
		String str = SoapUtil.getInstance().generateSoapMsgStr(accessInfo, container);
		
		//send request
		String response = HttpUtil.doPost(KssSystemConfig.URI, str);
		
		//response
		SOAPMessage msg = SoapUtil.getInstance().strXMLToSoapMsg(response);
		SOAPBody body = msg.getSOAPBody();
		Iterator<SOAPElement> iterator = body.getChildElements();
		return SoapUtil.getInstance().xmlToJson(iterator, "HardCancel");

			gregergerg


	}
	
	public String renew(Renew renew) throws Exception {
		String transactionId = UUID.randomUUID().toString();
		String nowTime = Instant.now().toString();

		//request string
		SubscriptionRequestContainer container = new SubscriptionRequestContainer(nowTime, transactionId, new SubscriptionRequest(renew));
		AccessInfo accessInfo = new AccessInfo (KssSystemConfig.ACCESS_INFO_NAME, KssSystemConfig.ACCESS_INFO_PWD);
		String str = SoapUtil.getInstance().generateSoapMsgStr(accessInfo, container);
		
		//send request
		String response = HttpUtil.doPost(KssSystemConfig.URI, str);
		
		//response
		SOAPMessage msg = SoapUtil.getInstance().strXMLToSoapMsg(response);
		SOAPBody body = msg.getSOAPBody();
		Iterator<SOAPElement> iterator = body.getChildElements();
		return SoapUtil.getInstance().xmlToJson(iterator, "Renew");
	}

	public String getInfo(@Valid GetInfo getInfo) throws Exception {
		String transactionId = UUID.randomUUID().toString();
		String nowTime = Instant.now().toString();

		//request string
		SubscriptionRequestContainer container = new SubscriptionRequestContainer(nowTime, transactionId, new SubscriptionRequest(getInfo));
		AccessInfo accessInfo = new AccessInfo (KssSystemConfig.ACCESS_INFO_NAME, KssSystemConfig.ACCESS_INFO_PWD);
		String str = SoapUtil.getInstance().generateSoapMsgStr(accessInfo, container);
		
		//send request
		String response = HttpUtil.doPost(KssSystemConfig.URI, str);
		
		//response
		SOAPMessage msg = SoapUtil.getInstance().strXMLToSoapMsg(response);
		SOAPBody body = msg.getSOAPBody();
		Iterator<SOAPElement> iterator = body.getChildElements();
		return SoapUtil.getInstance().xmlToJson(iterator, "GetInfo");
	}
	
}
